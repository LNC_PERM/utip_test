(() => {
  const domElements = {
    container: document.querySelector('.dyndata-container'),
    preloader: {
      elem: document.querySelector('.preloader'),
      progress: document.querySelector('.preloader__progress'),
      show: function (loadPercent) {
        this.elem.classList.remove('hidden');
        this.progress.style.width = `${loadPercent}%`;
      },
      hide: function () {
        this.elem.classList.add('hidden');
      }
    },
    loadBtn: document.querySelector('#load-btn'),
    clearBtn: document.querySelector('#clear-btn'),
    removeBtn: document.querySelector('#remove-row-btn'),
    message: {
      elem: document.querySelector('.message'),
      show: function () {
        this.elem.classList.remove('hidden');
      },
      hide: function () {
        this.elem.classList.add('hidden');
      }
    }
  }

  const LOADED_DATA_STORAGE = 'loaded-data';
  const PAGES_COUNT_STORAGE = 'pages-count';
  const PAGE_LENGTH = 20;
  let currPage = 1;

  const dataTable = {
    data: [],
    pagesCount: 1,

    save: function () {
      localStorage.setItem(LOADED_DATA_STORAGE, JSON.stringify(this.data));
      localStorage.setItem(PAGES_COUNT_STORAGE, this.pagesCount);
    },

    open: function () {
      this.pagesCount = Number(localStorage.getItem(PAGES_COUNT_STORAGE)) || 1;
      const dataS = localStorage.getItem(LOADED_DATA_STORAGE);
      this.data = dataS ? JSON.parse(dataS) : [];
      this.show();
    },

    clear: function () {
      this.data = [];
      this.pagesCount = 0;
      this.save();
      this.show();
    },

    show: function () {
      const showData = this.data.slice((currPage - 1) * PAGE_LENGTH, currPage * PAGE_LENGTH);
      domElements.container.innerHTML = '';
      (this.pagesCount > 1) && showPagination();
      if (this.data.length) {
        const table = document.createElement('table');
        table.classList.add('table');

        const fields = { name: 'Название', population: 'Население', climate: 'Климат', terrain: 'Территория', surface_water: 'Процент воды' };
        const tableHLine = document.createElement('tr');
        const tHead = document.createElement('thead');
        tHead.id = 'thead';
        for (const [name, descr] of Object.entries(fields)) {
          const th = document.createElement('th');
          th.innerText = descr;
          th.dataset.name = name;
          tableHLine.append(th);
        }
        tHead.append(tableHLine);
        table.append(tHead);
        const tBody = document.createElement('tbody');
        tBody.id = 'tbody';
        for (const i in showData) {
          const tableLine = document.createElement('tr');
          tableLine.dataset['id'] = Number(i) + (currPage - 1) * PAGE_LENGTH;
          for (const field in fields) {
            const td = document.createElement('td');
            td.innerText = showData[i][field];
            tableLine.append(td);
          }
          tBody.append(tableLine);
        }
        table.append(tBody);
        domElements.container.append(table);
        this.bindEvents();
        setColWidth(tHead, tBody);
      }
      domElements.clearBtn.disabled = !this.data.length;
      domElements.removeBtn.disabled = true;
    },

    bindEvents: function () {
      const tBody = domElements.container.querySelector('#tbody');
      tBody.addEventListener('click', (event) => {
        const target = event.target;
        if (target.tagName != 'TD') return;
        const row = target.closest('tr');
        const isSelected = row.classList.contains('selected');
        tBody.querySelectorAll('tr.selected').forEach(item => { item.classList.remove('selected') });
        if (!isSelected) {
          row.classList.add('selected');
        };
        domElements.removeBtn.disabled = isSelected;
      });
      const tHead = domElements.container.querySelector('#thead');
      tHead.addEventListener('click', (event) => {
        const target = event.target;
        if (target.tagName != 'TH') return;
        const fieldName = target.dataset.name;
        if (['name', 'terrain', 'climate'].includes(fieldName)) {
          this.data.sort((a, b) => {
            return (a[fieldName]).toLowerCase() > (b[fieldName]).toLowerCase() ? 1 : -1;
          });
        } else {
          const toNumber = function (n) {
            const num = Number(n);
            return isNaN(num) ? -1 : num;
          }
          this.data.sort((a, b) => {
            return (toNumber(a[fieldName]) < toNumber(b[fieldName])) ? 1 : -1;
          });
        }
        currPage = 1;
        this.show();
        this.save();
      })
    },

    removeRow: function () {
      const id = tbody.querySelector('.selected').dataset.id;
      this.data.splice(id, 1);
      this.show();
      this.save();
    }
  };

  async function loadData() {
    dataTable.data = [];
    domElements.container.innerHTML = '';
    let page = 0;
    let loadPercent = 0;
    let apiPagesCount = 0;
    let apiUrl = `https://swapi.dev/api/planets/?page=1`;
    domElements.message.hide();
    domElements.preloader.show(loadPercent);
    while (apiUrl) {
      const response = await fetch(apiUrl);
      const data = await response.json();
      apiUrl = data.next;
      dataTable.data = dataTable.data.concat(data.results);
      apiUrl && page++;
      (page === 1) && (apiPagesCount = Math.ceil(data.count / data.results.length));
      if (page) {
        loadPercent = Math.round(page / apiPagesCount * 100);
        domElements.preloader.show(loadPercent);
      }
    }
    domElements.preloader.hide();
    currPage = 1;
    dataTable.pagesCount = Math.ceil(dataTable.data.length / PAGE_LENGTH);
    dataTable.show();
    domElements.clearBtn.disabled = false;
    dataTable.save();
  }

  function setColWidth(tHead, tBody) {
    const colWidth = [];
    let i = 0;
    tBody.querySelector('tr').querySelectorAll('td').forEach(item => { colWidth.push(item.getBoundingClientRect().width) });
    tHead.querySelectorAll('th').forEach(item => { item.style.width = colWidth[i++] + 'px' });
  }

  function showPagination() {
    const VIEW_COUNT = 11;
    let pageMin = currPage - Math.floor(VIEW_COUNT / 2);
    (pageMin < 1) && (pageMin = 1);
    let pageMax = pageMin + VIEW_COUNT - 1;
    (pageMax > dataTable.pagesCount) && (pageMax = dataTable.pagesCount);

    const ul = document.createElement('ul');
    ul.classList.add('pagination-list');

    if (pageMin !== 1) {
      ul.append(createPaginationElement(1));
      ul.append(createPaginationDots());
    }
    for (let i = pageMin; i <= pageMax; i++) {
      ul.append(createPaginationElement(i));
    }
    if (pageMax !== dataTable.pagesCount) {
      ul.append(createPaginationDots());
      ul.append(createPaginationElement(dataTable.pagesCount));
    }
    domElements.container.prepend(ul);
    ul.addEventListener('click', (event) => {
      const target = event.target;
      if (target.tagName != 'BUTTON') return;
      currPage = parseInt(target.innerText);
      showPagination();
      dataTable.show();
    });
  }

  function createPaginationElement(page) {
    const li = document.createElement('li');
    li.classList.add('pagination-item');
    const btn = document.createElement('button');
    btn.innerText = page;
    btn.classList.add('pagination-link');
    (currPage === page) && btn.classList.add('current-page');
    li.append(btn);
    return li;
  }

  function createPaginationDots() {
    const dots = document.createElement('li');
    dots.classList.add('pagination-dots');
    dots.innerText = '...';
    return dots;
  }

  document.addEventListener("DOMContentLoaded", () => {
    domElements.loadBtn.addEventListener('click', () => {
      loadData();
    })
    domElements.clearBtn.addEventListener('click', () => {
      dataTable.clear();
      domElements.message.show();
      domElements.removeBtn.disabled = true;
    })
    domElements.removeBtn.addEventListener('click', () => {
      dataTable.removeRow();
      domElements.removeBtn.disabled = true;
    })

    dataTable.open();
    dataTable.data.length || domElements.message.show();
  });

})();
